from turtle import *

def spirale(longueur):
    if longueur>20:
        forward(longueur)
        left(90)
        forward(longueur)
        left(90)
        spirale(longueur-20)

spirale(300)
exitonclick()



